# -*- encoding: utf-8 -*-
from django.forms import widgets
from rest_framework import serializers
from api.models import *
from django.contrib.auth.models import User

class FriendshipSerializers(serializers.Serializer):

	class Meta:
		model = Friendship
		fields = ('id' , 'userId', 'userIdSec')

	id = serializers.IntegerField()
	userId = serializers.CharField()
	userIdSec = serializers.CharField()

class EventSerializers(serializers.Serializer):
	class Meta:
		model = Event
		fields = ('id','name','place','beginHour','endHour','picture','description','price','address','font','beginDate','endDate','category','contact','pageURL','latitude','longitude','eventURL','grade')

	id = serializers.IntegerField()
	name = serializers.CharField(max_length = 100)
	place = serializers.CharField(max_length = 400)
	beginHour = serializers.TimeField()
	endHour = serializers.TimeField()
	picture = serializers.URLField(max_length = 600)
	description = serializers.CharField(max_length = 500)
	price = serializers.DecimalField(default = 0.0, max_digits = 20, decimal_places = 2)
	address = serializers.CharField(max_length = 500)
	font = serializers.CharField(max_length = 100)
	beginDate = serializers.DateField()
	endDate = serializers.DateField()
	category = serializers.CharField(max_length = 100)
	contact = serializers.CharField(max_length = 50)
	pageURL = serializers.URLField(max_length = 600)
	latitude = serializers.CharField(max_length = 50)
	longitude = serializers.CharField(max_length = 50)
	eventURL =  serializers.URLField(max_length = 600)
	grade = serializers.IntegerField()	

	def restore_object(self, attrs, instance=None):
		if instance:

			instance.id = attrs.get('id', instance.id)
			instance.name = attrs.get('name', instance.name)
			instance.place = attrs.get('place', instance.place)
			instance.beginHour = attrs.get('beginHour', instance.beginHour)
			instance.endHour = attrs.get('endHour', instance.endHour)
			instance.picture = attrs.get('picture', instance.picture)
			instance.address = attrs.get('address', instance.address)
			instance.font = attrs.get('font', instance.font)
			instance.beginDate = attrs.get('beginDate', instance.beginDate)
			instance.endDate = attrs.get('endDate', instance.endDate)
			instance.category = attrs.get('category', instance.category)
			instance.contact = attrs.get('contact', instance.contact)
			instance.pageURL = attrs.get('pageURL', instance.pageURL)
			instance.latitude = attrs.get('latitude', instance.latitude)
			instance.longitude = attrs.get('longitude', instance.longitude)
			instance.eventURL = attrs.get('eventURL', instance.eventURL)
			instance.grade = attrs.get('grade', instance.grade)
			return instance

		return Event(**attrs)

class Event_UserSerializers(serializers.Serializer):
	class Meta:
		model = Event_User
		fields = ('id', 'eventId','userId','Favorito')

	id = serializers.IntegerField()
	eventId = serializers.CharField()
	userId = serializers.CharField()
	Favorito = serializers.BooleanField()

	def restore_object(self, attrs, instance=None):
		if instance:
			instance.id = attrs.get('id', instance.id)
			instance.eventId = attrs.get('eventId', instance.eventId)
			instance.userId = attrs.get('userId', instance.userId)
			instance.Favorito = attrs.get('Favorito', instance.Favorito)
			return instance
		return Event_User(**attrs)	

class UserSerializers(serializers.Serializer):
	class Meta:
		model = User
		fields = ('id', 'username','first_name')

	id = serializers.IntegerField()
	username = serializers.CharField(max_length = 30)
	first_name = serializers.CharField(max_length = 30)

	def restore_object(self, attrs, instance=None):
		if instance:
			instance.id = attrs.get('id', instance.id)
			instance.username = attrs.get('username', instance.username)
			instance.first_name = attrs.get('first_name', instance.first_name)
			return instance