from django.conf.urls import patterns, url

urlpatterns = patterns('api.views',
    url(r'^get/todos_eventos$', 'showEverything'),
    #url(r'^get/Relaciones_User-Event(?P<pk>)/$', 'Relations'),
    #url(r'^Full/', 'fullDatabase'),
    url(r'^get/event_by_id/(\d+)/$', 'showById'),
    url(r'^post/user_register/$', 'userRegister'),
    url(r'^get/user_by_id/(\d+)/$', 'showUserById'),
    url(r'^get/event_user_by_id/(\d+)/$', 'getEvent_User'),
    url(r'^put/change_favorite/(\d+)/$', 'change_favorite'),
)