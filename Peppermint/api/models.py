from django.db import models
from django.contrib.auth.models import User

class Friendship(models.Model):
	userId = models.ForeignKey(User, related_name = 'user')
	userIdSec = models.ForeignKey(User, related_name = 'friend')

	def __unicode__(self):
		return unicode(str(self.userId))


class Event(models.Model):
	name = models.CharField(max_length = 100)
	place = models.CharField(max_length = 400)
	beginHour = models.CharField(max_length = 40)
	endHour = models.CharField(max_length = 40)
	picture = models.URLField(max_length = 600)
	description = models.CharField(max_length = 500)
	price = models.CharField(max_length=30)
	address = models.CharField(max_length = 500)
	font = models.CharField(max_length = 100)
	beginDate = models.CharField(max_length = 40)
	endDate = models.CharField(max_length = 40)
	category = models.CharField(max_length = 100)
	contact = models.CharField(max_length = 50)
	pageURL = models.URLField(max_length = 600)
	latitude = models.CharField(max_length = 50)
	longitude = models.CharField(max_length = 50)
	eventURL =  models.URLField(max_length = 600)
	grade = models.IntegerField()

	def __unicode__(self):
		return unicode(str(self.id))

class Event_User(models.Model):
	eventId = models.ForeignKey(Event)
	userId = models.ForeignKey(User)
	Favorito = models.BooleanField(default = False)

	def __unicode__(self):
		return unicode(str(self.id))

