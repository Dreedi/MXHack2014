# -*- encoding: utf-8 -*-
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from api.models import *
from api.serializers import *
from urllib2 import urlopen
import json
import unicodedata
from django.contrib.auth.models import User
from decimal import * 


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

@csrf_exempt
def showEverything(request):
	if request.method == 'GET':
		Events = Event.objects.all()
		serializer = EventSerializers(Events, many = True)
		return JSONResponse(serializer.data)


@csrf_exempt
def showById(request, ids):
	Events = get_object_or_404(Event, id = int(ids))
	serializer = EventSerializers(Events, many = False)
	return JSONResponse(serializer.data)

@csrf_exempt
def userRegister(request):
	NewUser = User()
	if request.method == 'POST':
		id_fb = request.POST.get('id_fb','')
		name = request.POST.get('name','')
		print id_fb
		NewUser = User.objects.create_user(id_fb, email=None, password = None)
		NewUser.first_name = name
		NewUser.save()
		return HttpResponse('Realized')
	return HttpResponse('Without POST method')

@csrf_exempt
def showUserById(request, ids):
	Users = get_object_or_404(User, id = int(ids))
	dicctionary = {"id":Users.id, "username": Users.username, "first_name": Users.first_name }
	return JSONResponse(dicctionary)

@csrf_exempt
def getEvent_User(request, ids):

	Relations = Event_User.objects.filter(userId = int(ids))
	if len(Relations) > 0:
		serializer = Event_UserSerializers(Relations, many = True)
		return JSONResponse(serializer.data)
	else:
		raise Http404

@csrf_exempt
def change_favorite(request, ids):
	if request.method == 'PUT':
		Relation = get_object_or_404(Event_User, id = int(ids))
		Relation.Favorito = Relation.Favorito == False
		serializer = Event_UserSerializers(Relation, many = False)
		Relation.save()
		return JSONResponse(serializer.data)


#def fullDatabase(request):
	#url = 'http://eventario.mx/eventos.json'
	#JSON = urlopen(url).read()
	#answer = json.loads(JSON)

	#a = Event()

	#for i in answer:
	#	a.name = (i["nombre"].encode('utf-8'))
	#	a.place = (i["lugar"].encode('utf-8'))
	#	a.beginHour = (i["hora_inicio"].encode('utf-8'))[11:-4]
	#	a.endHour = (i["hora_fin"].encode('utf-8'))[11:-4]
	#	a.picture = (i["imagen"].encode('utf-8'))
	#	a.description = (i["descripcion"].encode('utf-8'))
	#	a.price = (i["precio"].encode('utf-8'))
	#	a.address = (i["direccion"].encode('utf-8')) 
	#	a.font = (i["fuente"].encode('utf-8'))
	#	a.beginDate = (i["fecha_inicio"].encode('utf-8'))
	#	a.endDate = (i["fecha_fin"].encode('utf-8'))
	#	a.category = (i["categoria"].encode('utf-8'))
	#	a.contact = (i["contacto"].encode('utf-8'))
	#	a.pageURL = (i["pagina"].encode('utf-8'))
	#	a.latitude = (i["latitud"])
	#	a.longitude = (i["longitud"])
	#	try:
	#		a.eventURL = (i["distancia"].encode('utf-8'))
	#	except Exception, e:
	#		a.eventURL = u"None"
	#	a.grade = 10
	#	a.save()
	#	a = Event()

	#return HttpResponse('Hola')