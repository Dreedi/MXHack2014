# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('place', models.CharField(max_length=400)),
                ('beginHour', models.TimeField()),
                ('endHour', models.TimeField()),
                ('picture', models.URLField(max_length=600)),
                ('description', models.CharField(max_length=500)),
                ('price', models.DecimalField(default=0.0, max_digits=20, decimal_places=2)),
                ('address', models.CharField(max_length=500)),
                ('font', models.CharField(max_length=100)),
                ('beginDate', models.DateField()),
                ('endDate', models.DateField()),
                ('category', models.CharField(max_length=100)),
                ('contact', models.CharField(max_length=50)),
                ('pageURL', models.URLField(max_length=600)),
                ('latitude', models.CharField(max_length=50)),
                ('longitude', models.CharField(max_length=50)),
                ('eventURL', models.URLField(max_length=600)),
                ('grade', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Event_User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Favorito', models.BooleanField(default=False)),
                ('eventId', models.ForeignKey(to='api.Event')),
                ('userId', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Friendship',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('userId', models.ForeignKey(related_name='user', to=settings.AUTH_USER_MODEL)),
                ('userIdSec', models.ForeignKey(related_name='friend', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
