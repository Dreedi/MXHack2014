# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('principal', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='place',
            name='description',
        ),
        migrations.AlterField(
            model_name='event',
            name='description',
            field=models.CharField(default=b'', max_length=500),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='place',
            name='schedule',
            field=models.CharField(default=b'', max_length=50),
            preserve_default=True,
        ),
    ]
