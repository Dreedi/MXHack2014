from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Place(models.Model):
	name = models.CharField(max_length=100)
	price = models.DecimalField(default=0.0, max_digits=20, decimal_places=2)
	schedule = models.CharField(default=" ", max_length=50)
	address = models.CharField(max_length=200)

	def __unicode__(self):
		return self.name

class Event(models.Model):
	name = models.CharField(max_length=100)
	description = models.CharField(default=" ", max_length=500)
	price = models.DecimalField(default=0.0, max_digits=20, decimal_places=2)
	schedule = models.CharField(max_length=50)
	capacity = models.IntegerField()
	place = models.ForeignKey(Place)

	def __unicode__(self):
		return self.name