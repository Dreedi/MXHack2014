from django.contrib import admin
from models import Event, Place
# Register your models here.

@admin.register(Place)
class PlaceAdmin(admin.ModelAdmin):
	list_display = ('id', 'name', 'price', 'schedule', 'address')
	search_fields = ('id', 'name', 'description', 'price', 'schedule', 'address')

@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
	list_display = ('id', 'name', 'description', 'price', 'schedule', 'capacity', 'place')
	search_fields = ('id', 'name', 'description', 'price', 'schedule', 'capacity', 'place')